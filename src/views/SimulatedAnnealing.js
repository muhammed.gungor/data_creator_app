import React from "react";
import { Form, Input, Button, Card, InputNumber, Row, Col, Divider, Progress, notification, Spin, Modal } from 'antd';
import { Container } from 'reactstrap';
import { observer } from 'mobx-react';
import SimulatedAnnealingStore from "../stores/SimulatedAnnealingStore";


const openNotificationWithIcon = (type, message, duration = 5) => {
    notification.config({
        placement: 'bottomRight',
        top: 20,
        duration: duration,
    })
    notification[type]({
        message: 'Nice!',
        description:
            message
    });
};


const SimulatedAnnealing = observer(
    class SimulatedAnnealing extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                reverse: false,
                inputValue: 1,
                headers: [],
                csvData: [],
                percent: 0,
                selectedModel: "linear",
                isSpinning: false,
                visible: false,
                regressionFuntion: null
            };
        }

        csvLink = React.createRef()

        static defaultProps = {
            ...this.props
        }

        randomIntFromInterval = (min, max) => { // min and max included 
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min;
        }

        randomFloatFromInterval = (min, max) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            return (Math.random() * (max - min)) + min;
        }


        componentDidMount = () => {

        }


        checkValueZero = (rule, value) => {
            if (value < 0 || value > 0) {
                return Promise.resolve();
            }
            return Promise.reject(rule.field + ' must be greater or less than than zero!');
        };

        checkValueLessThanZero = (rule, value) => {
            if (value < 0) {
                return Promise.resolve();
            }
            return Promise.reject(rule.field + ' must be greater than zero!');
        };

        calculateWithFxFunction = (t_value, min_t_value, innerIterationCount, temperatureDecreaseAmount, minimumRange, maximumRange) => {
            var calculatedList = [];

            let temporaryInnerIterationCount = innerIterationCount;
            var initialValue = this.executeFxFunction(minimumRange, maximumRange).fxFunction;

            while (t_value > min_t_value) {
                innerIterationCount = temporaryInnerIterationCount;

                while (innerIterationCount > 0) {
                    var executedResult = this.executeFxFunction(minimumRange, maximumRange)
                    let currentValue = executedResult.fxFunction
                    let delta = currentValue - initialValue

                    let r_value = Math.random();

                    if (delta < 0 || (Math.exp(((-delta) / t_value)) > r_value)) {
                        initialValue = currentValue

                        calculatedList.push({
                            initialValue: initialValue,
                            x1: executedResult.x1,
                            x2: executedResult.x2
                        })
                    }

                    innerIterationCount--;
                }

                t_value = this.decreaseTemperatureValue(t_value, temperatureDecreaseAmount)
            }

            this.setState({
                isSpinning: false
            })

            return calculatedList;
        }

        decreaseTemperatureValue = (t_value, decreaseAmount) => {
            return t_value * decreaseAmount;
        }

        executeFxFunction = (min, max) => {
            let x1 = this.randomFloatFromInterval(min, max);
            let x2 = this.randomFloatFromInterval(min, max);

            var fxFunction = (4 - 2 * Math.pow(x1, 2) + Math.pow(x1, 4) / 3) * Math.pow(x1, 2) + x1 * x2 + (-4 + 4 * Math.pow(x2, 2)) * Math.pow(x2, 2);

            return { fxFunction, x1, x2 };
        }

        onFinish = values => {
            this.setState({
                isSpinning: true,
            })
            if (values) {
                if (Number(values.minimumRange) >= Number(values.maximumRange)) {
                    openNotificationWithIcon("error", "Max values must be greater than Min values.")
                }
                else {
                    try {
                        setTimeout(() => {
                            let t_value = values.t_value;
                            let innerIterationCount = values.innerIterationCount;
                            let temperatureDecreaseAmount = values.temperatureDecreaseAmount;
                            let minimumRange = values.minimumRange;
                            let maximumRange = values.maximumRange;
                            let min_t_value = values.min_t_value;

                            let initialValue = this.calculateWithFxFunction(t_value, min_t_value, innerIterationCount, temperatureDecreaseAmount, minimumRange, maximumRange);                            

                            this.findMinAndMax(initialValue)

                        }, 300);
                    } catch (error) {
                        openNotificationWithIcon("error", error)
                    }
                }
            }
        };

        findMinAndMax = (array) => {
            var lowest = Number.POSITIVE_INFINITY;
            var highest = Number.NEGATIVE_INFINITY;
            var val, x1, x2;
            for (var i = array.length - 1; i >= 0; i--) {
                val = array[i].initialValue;
                if (val < lowest) {
                    lowest = val;
                    let item = array[i];
                    x1 = item.x1;
                    x2 = item.x2
                }
                if (val > highest) {
                    highest = val;
                }
            }
            console.log(highest, lowest);

            var minVals = { 
                currentValue: lowest, 
                x1: x1, 
                x2: x2 
            }

            SimulatedAnnealingStore.setMinValues(minVals);

            openNotificationWithIcon("success", "Algorithm annealed with successfully. ", 0)
        }

        onFinishFailed = errorInfo => {
            this.setState({ showProgress: false })
            console.log('Failed:', errorInfo);
        };


        showModal = () => {
            this.setState({
                visible: true,
            });
        };

        handleOk = e => {
            console.log(e);
            this.setState({
                visible: false,
            });
        };

        handleCancel = e => {
            console.log(e);
            this.setState({
                visible: false,
            });
        };

        render() {
            const layout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 }
                },
            };

            const tailLayout = {
                wrapperCol: {
                    xs: {
                        span: 16,
                        offset: 0,
                    },
                    sm: {
                        span: 16,
                        offset: 4,
                    },
                }
            };

            return <Container style={{ ...this.props.style }}>
                <Spin spinning={this.state.isSpinning} tip="Calculating... Please Wait">
                    <Card style={{ width: "100%" }}>
                        <Form hideRequiredMark="true" {...layout} name="basic" initialValues={{ remember: true, x_from: 0, x_to: 0, y_from: 0, y_to: 0 }} onFinishFailed={this.onFinishFailed} onFinish={this.onFinish} >
                            <Divider orientation="left">Simulated Annealing</Divider>
                            <Form.Item
                                label="T Value"
                                name="t_value"
                                rules={[{ required: true, message: 'Please input T Value!' }]}
                            >
                                <InputNumber size="middle" style={{ width: 250 }} min={2} defaultValue={5} />
                            </Form.Item>

                            <Form.Item
                                label="Inner Ite. Count"
                                name="innerIterationCount"
                                rules={[{ required: true, message: 'Please input Inner Count!' }]}
                            >
                                <InputNumber size="middle" style={{ width: 250 }} defaultValue={-5} />
                            </Form.Item>

                            <Form.Item
                                label="T Decrease Val."
                                name="temperatureDecreaseAmount"
                                rules={[{ required: true, message: 'Please input T Decrease Amount!' }]}
                            >
                                <InputNumber size="middle" max={0.99} min={0.86} style={{ width: 250 }} defaultValue={-5} />
                            </Form.Item>

                            <Form.Item
                                label="Min Temperature"
                                name="min_t_value"
                                rules={[{ required: true, message: 'Please input Minimum Temperature!' }]}
                            >
                                <InputNumber size="middle" style={{ width: 250 }} defaultValue={-5} />
                            </Form.Item>

                            <Divider orientation="left">Range</Divider>

                            <Row style={{ display: "flex", flexDirection: "row", justifyContent: "space-evenly", alignContent: "center" }}>
                                <Col span={12}>
                                    <Form.Item wrapperCol={{
                                        xs: { span: 24 },
                                        sm: { span: 20 }
                                    }} name="minimumRange" rules={[{ validator: this.checkValueZero }]}>
                                        <Input addonBefore="Min" />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item wrapperCol={{
                                        xs: { span: 24 },
                                        sm: { span: 22 }
                                    }} name="maximumRange" rules={[{ validator: this.checkValueZero }]}>
                                        <Input addonBefore="Max" />
                                    </Form.Item>
                                </Col>
                            </Row>

                            <Form.Item {...tailLayout} style={{ marginTop: 40 }}>
                                <Button type="primary" htmlType="submit">
                                    Calculate
                                </Button>
                                {false ? <Progress percent={this.state.percent} /> : null}
                            </Form.Item>

                        </Form>
                    </Card>

                </Spin>
                <Modal
                    title="Regression Function Detail"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    {this.state.regressionFuntion}
                </Modal>
            </Container>
        }
    });

export default SimulatedAnnealing;

