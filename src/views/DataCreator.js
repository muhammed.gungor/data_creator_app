import React from "react";
import { Form, Input, Button, Card, InputNumber, Row, Col, Select, Divider, Progress, notification, Spin, Modal } from 'antd';
import { Container } from 'reactstrap';
import { observer } from 'mobx-react';
import { CSVLink } from "react-csv";
import * as math from 'mathjs';
import Text from "antd/lib/typography/Text";

const { Option } = Select;

const openNotificationWithIcon = (type, message) => {
    notification.config({
        placement: 'topRight',
        top: 20,
        duration: 5,
    })
    notification[type]({
        message: 'Please..',
        description:
            message
    });
};


const openExcelNotification = (csvData, headers) => {
    const key = `open${Date.now()}`;
    const btn = (

        csvData.length > 0 ? <CSVLink data={csvData} headers={headers}
            filename={"data_" + new Date().getTime() + ".csv"}
            target="_blank" separator={";"}
            //ref={this.csvLink}
            className="hidden"
        >
            Download CSV File
</CSVLink> : null

    );
    notification.config({
        placement: 'bottomRight',
        bottom: 50,
        duration: 0,
    })
    notification.open({
        message: 'Success',
        description:
            'Your data has been generated. Use anywhere!',
        btn,
        key,
        type: "success"
    });
};

const DataCreator = observer(
    class DataCreator extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                reverse: false,
                inputValue: 1,
                headers: [],
                csvData: [],
                percent: 0,
                selectedModel: "linear",
                isSpinning: false,
                visible: false,
                regressionFuntion: null
            };
        }

        csvLink = React.createRef()

        static defaultProps = {
            ...this.props
        }

        increase = (value) => {
            let percent = this.state.percent + value;
            if (percent > 100) {
                percent = 100;
            }
            this.setState({ percent });
        };

        decline = () => {
            let percent = this.state.percent - 10;
            if (percent < 0) {
                percent = 0;
            }
            this.setState({ percent });
        };

        handleReverseChange = reverse => {
            this.setState({ reverse });
        };

        onChange = value => {
            this.setState({
                inputValue: value,
            });
        };

        randomIntFromInterval = (min, max) => { // min and max included 
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min;
        }

        randomFloatFromInterval = (min, max) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            return (Math.random() * (max - min)) + min;
        }

        generateOutlinerData = (outlinerCount, data_to, data_from, correlation) => {
            let randomOutlinerData = [];
            let dataDiff = Number(data_to) - Number(data_from)

            let outlinerDataMax = Number(data_to) + (dataDiff * 2);

            if (correlation) {
                for (let i = 0; i < outlinerCount; i++) {
                    let random = this.randomIntFromInterval(data_to, outlinerDataMax) * this.randomFloatFromInterval(0.5, 1) * correlation;
                    console.log("outliner data:", random)
                    randomOutlinerData.push(random)
                }
            }
            else {
                for (let i = 0; i < outlinerCount; i++) {
                    let random = this.randomIntFromInterval(data_to, outlinerDataMax);
                    console.log("outliner data:", random)
                    randomOutlinerData.push(random)
                }
            }

            this.increase(10)

            return randomOutlinerData;
        }

        dataCreateProcessForNonLinear = (values) => {
            this.increase(5)
            let data = [];
            let headers = [];

            headers.push(
                {
                    label: "Y Values",
                    key: "yValues"
                }
            );

            for (let i = 0; i < values.feature; i++) {
                headers.push(
                    {
                        label: "Feature " + (i + 1),
                        key: "feature" + (i + 1)
                    }
                );
            }

            for (let i = 0; i < values.sample; i++) {
                //Non Lineear
                var y_value = this.randomIntFromInterval(values.y_from, values.y_to);
                console.log(values.y_from, values.y_to, y_value)
                //Linear
                // Y = C0 + C1.X1 + C2.X2

                var featureArray = {};

                featureArray["yValues"] = y_value;

                for (let j = 0; j < values.feature; j++) {
                    var featureValue = this.randomIntFromInterval(values.x_from, values.x_to);
                    console.log(values.x_from, values.x_to, featureValue)
                    var key = "feature" + (j + 1)
                    featureArray[key] = featureValue;
                }

                data.push(featureArray);
            }
            this.increase(25)
            console.log("state headers:", this.state.headers)
            console.log("state csvData:", this.state.csvData)
            var contract = {
                data: data,
                headers: headers
            }

            return contract
        }

        computeStandardDeviation = (array) => {
            return math.std(array);
        }

        computeZScores = (array) => {
            let standartDeviation = this.computeStandardDeviation(array);
            console.log("standartDeviation", standartDeviation)
            let mean = math.mean(array);

            for (let i = 0; i < array.length; i++) {
                array[i] = (array[i] - mean) / standartDeviation;
            }

            return array;
        }

        computeMinMax = (array) => {
            let minValue = math.min(array)
            console.log("min x:", minValue)
            let maxValue = math.max(array);
            console.log("max x:", maxValue)

            for (let i = 0; i < array.length; i++) {
                let top = (array[i] - minValue);
                let bottom = (maxValue - minValue);
                array[i] = parseFloat(top / bottom)
            }

            return array;
        }

        componentDidMount = () => {

        }

        dataGenerate = (values) => {
            // Önce Matrisi oluştur.
            console.log("datagenerate")
            let matrix = [];

            for (let i = 0; i < values.sample; i++) {
                // şimdi ilk satır mesela,
                // İlgili aralıklarla, sample sayısı kadar random data üret ve bekle.
                let rowValues = [];

                for (let j = 0; j < values.feature; j++) {
                    let cValue = this.randomFloatFromInterval(1, 2);
                    var randomValue = this.randomIntFromInterval(values.x_from, values.x_to) * values.correlation * Number((cValue).toFixed(3));

                    rowValues.push(randomValue);
                }


                if (this.state.selectedNormalization === "zScore") {
                    rowValues = this.computeZScores(rowValues);
                    matrix.push(rowValues)
                }
                else {
                    rowValues = this.computeMinMax(rowValues);
                    matrix.push(rowValues)
                }
            }

            let yValues = this.correlationComputingForYValues(matrix, values)
            this.prepareExcelData(yValues, matrix)

            let oneMatrix = [];

            for (let i = 0; i < matrix.length; i++) {
                oneMatrix.push(1);
            }


            let temporaryMatix = math.transpose(matrix);
            temporaryMatix.splice(0, 0, oneMatrix);
            matrix = math.transpose(temporaryMatix); //En sol sutün 1 lerle dolu olmalı

            let betaValues = this.computeRegressionCoefficents(matrix, yValues);
            this.prepareRegressionFunction(betaValues)

            setTimeout(() => {
                openExcelNotification(this.state.csvData, this.state.headers);
            }, 200);
        }

        correlationComputingForYValues = (matrix, values) => {
            let yValues = [];

            for (let i = 0; i < matrix.length; i++) {
                let yValue = 0;

                yValue = this.randomIntFromInterval(values.y_from, values.y_to);
                yValues.push(yValue);
            }

            return yValues;
        }

        prepareExcelData = (yValues, matrix) => {
            let headers = ["Y Values"];
            let transposedArray = math.transpose(matrix)
            transposedArray.splice(0, 0, yValues)
            matrix = math.transpose(transposedArray)

            for (let i = 1; i < matrix[0].length; i++) {
                let text = "Feature " + i;
                headers.push(text);
            }
            console.log("headers:", headers);
            console.log("matrix with headers:", matrix)
            this.setState({
                csvData: matrix,
                headers: headers
            })
        }

        //Based this formule ----> Coefficents = (X'X)^-1 * X'Y
        computeRegressionCoefficents = (xMatrix, yMatrix) => {
            // X'X = x_valuesResult.
            // X'Y = x_y_valuesResult.   

            //Compute X'X
            let xTransposed = math.transpose(xMatrix);
            console.log("xTransposed with one matrix,(Ilk satir hep 1 olmali)", xTransposed)
            let multipled = math.multiply(xTransposed, xMatrix);
            console.log("multipled X'X:", multipled)
            try {
                let x_valuesResult = math.inv(multipled);
                console.log("x_valuesResult with Inverse:", x_valuesResult)

                //Compute X'Y
                let x_y_valuesResult = math.multiply(xTransposed, yMatrix);
                console.log("x_y_valuesResult ---> X'Y:", x_y_valuesResult)

                let betaValues = math.multiply(x_valuesResult, x_y_valuesResult);
                console.log("betaValues:", betaValues)

                return betaValues;
            } catch (error) {
                this.setState({
                    regressionFuntion: "Function cannt calculate. Error:" + error
                })
                return []
            }
        }


        prepareRegressionFunction = (betaValues) => {
            let funcText = "y = " + betaValues[0] + " + ";

            for (let i = 1; i < betaValues.length; i++) {
                let beta = Number((betaValues[i]).toFixed(5)) + "X(" + i + ")" + (i + 1 === betaValues.length ? "" : " + ");
                funcText += beta;
            }
            console.log("Function:", funcText)
            this.setState({
                regressionFuntion: funcText,
                isSpinning: false
            })
        }

        dataCreateProcessForLinear = (values) => {
            this.increase(5);

            let data = [];
            let headers = [];
            let correlation = values.correlation;

            headers.push(
                {
                    label: "Y Values",
                    key: "yValues"
                }
            );

            for (let i = 0; i < values.feature; i++) {
                headers.push(
                    {
                        label: "Feature " + (i + 1),
                        key: "feature" + (i + 1)
                    }
                );
            }

            for (let i = 0; i < values.sample; i++) {

                var y_value = 0;//this.randomIntFromInterval(1, 5); //C0 degerini ilk olarak atadık.

                //Linear
                // Y = C0 + C1.X1 + C2.X2

                var featureArray = {};

                for (let j = 0; j < values.feature; j++) {

                    //Xi
                    var featureValue = this.randomIntFromInterval(values.x_from, values.x_to) * correlation * this.randomFloatFromInterval(0, 1);
                    y_value = y_value + (correlation * featureValue);
                    var key = "feature" + (j + 1)
                    featureArray[key] = featureValue;
                }

                featureArray["yValues"] = y_value;

                data.push(featureArray);
            }

            this.increase(25)

            console.log("state headers:", this.state.headers)
            console.log("state csvData:", this.state.csvData)
            var contract = {
                data: data,
                headers: headers
            }

            return contract
        }

        changeDataWithOutliner_X = (outlinerData, outlinerCount, featureCount) => {
            let tempArray = this.state.csvData;

            for (let i = 0; i < outlinerCount; i++) {
                let randomIndex = this.randomIntFromInterval(0, featureCount);

                tempArray[i]["feature" + randomIndex] = outlinerData[i];
            }
            this.setState({ csvData: tempArray })
            this.increase(25)
        }

        changeDataWithOutliner_Y = (outlinerData, outlinerCount, sampleCount) => {
            let tempArray = this.state.csvData;
            console.log("temparray:", tempArray)

            for (let i = 0; i < outlinerCount; i++) {
                let randomIndex = this.randomIntFromInterval(0, sampleCount);
                console.log("randomIndex:", randomIndex)
                console.log("tempArray[randomIndex] : ", tempArray[randomIndex])

                tempArray[randomIndex]["yValues"] = outlinerData[i];
                console.log("tempArray[randomIndex] : ", tempArray[randomIndex])
            }
            this.increase(25)
            this.setState({ csvData: tempArray })
        }

        checkValueZero = (rule, value) => {
            if (value > 0) {
                return Promise.resolve();
            }
            return Promise.reject(rule.field + ' must be greater than zero!');
        };

        onFinish = values => {
            if (values) {
                if (Number(values.x_from) >= Number(values.x_to) || Number(values.y_from) >= Number(values.y_to)) {
                    openNotificationWithIcon("error", "'To' values must be greater than 'From' values.")
                }
                else {
                    this.setState({
                        csvData: [],
                        headers: null,
                        isSpinning: true,
                        regressionFuntion: null
                    })

                    try {
                        setTimeout(() => {
                            this.dataGenerate(values);
                        }, 300);
                    } catch (error) {
                        openNotificationWithIcon("error", error)
                    }
                }
            } else {
                var contract = this.state.selectedModel === "linear" ? this.dataCreateProcessForLinear(values) : this.dataCreateProcessForNonLinear(values);

                this.setState({
                    csvData: contract.data,
                    headers: contract.headers
                })

                if (contract) {
                    if (values.x_outliner) {
                        var outlinerDataX = this.generateOutlinerData(values.x_outliner, values.x_to, values.x_from, values.correlation);
                        this.changeDataWithOutliner_X(outlinerDataX, values.x_outliner, values.feature)
                    }
                    if (values.y_outliner) {
                        var outlinerDataY = this.generateOutlinerData(values.y_outliner, values.y_to, values.y_from, values.correlation);
                        this.changeDataWithOutliner_Y(outlinerDataY, values.y_outliner, values.sample)
                    }
                }
            }
        };

        onFinishFailed = errorInfo => {
            this.setState({ showProgress: false })
            console.log('Failed:', errorInfo);
        };

        onChange = (value) => {
            this.setState({ showProgress: false })
            console.log(value)
            this.setState({
                selectedModel: value
            })
        }

        showModal = () => {
            this.setState({
                visible: true,
            });
        };

        handleOk = e => {
            console.log(e);
            this.setState({
                visible: false,
            });
        };

        handleCancel = e => {
            console.log(e);
            this.setState({
                visible: false,
            });
        };

        render() {
            const layout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 }
                },
            };

            const tailLayout = {
                wrapperCol: {
                    xs: {
                        span: 16,
                        offset: 0,
                    },
                    sm: {
                        span: 16,
                        offset: 4,
                    },
                }
            };

            return <Container style={{ ...this.props.style }}>
                <Spin spinning={this.state.isSpinning} tip="Data generating please wait..">
                    <Card style={{ width: "100%" }}>
                        <Form hideRequiredMark="true" {...layout} name="basic" initialValues={{ remember: true, x_from: 0, x_to: 0, y_from: 0, y_to: 0 }} onFinishFailed={this.onFinishFailed} onFinish={this.onFinish} >
                            <Divider orientation="left">Base Properties</Divider>
                            <Form.Item
                                label="Feature size"
                                name="feature"
                                rules={[{ required: true, message: 'Please input features!' }]}
                            >
                                <InputNumber size="middle" style={{ width: 250 }} min={2} defaultValue={5} />
                            </Form.Item>

                            <Form.Item
                                label="Sample size"
                                name="sample"
                                rules={[{ required: true, message: 'Please input sample!' }]}
                            >
                                <InputNumber size="middle" style={{ width: 250 }} min={1} defaultValue={10} />
                            </Form.Item>

                            {/* <Form.Item name="dataUnderlying" label="Data Underlying" rules={[{ required: true, message: 'Please select model!' }]}>
                            <Select placeholder="Select a model" onChange={this.onChange} style={{ width: 250 }}>
                                <Option value="linear">Linear</Option>
                                <Option value="nonLinear">Non-Linear</Option>
                            </Select>
                        </Form.Item> */}

                            <Form.Item style={{ display: this.state.selectedModel === "linear" ? "flex" : "none" }}
                                label="Correlation Coeff."
                                name="correlation"
                                rules={[{ required: true, message: 'Please input Correlation Coefficent!' }]}
                            >
                                <InputNumber size="middle" style={{ width: 250 }} min={-1} max={1} defaultValue={0.4} />
                            </Form.Item>

                            <Form.Item name="normalization" label="Normalization" rules={[{ required: true, message: 'Please select normalization!' }]}>
                                <Select placeholder="Select a normalization" onChange={(value) => this.setState({ selectedNormalization: value })} style={{ width: 250 }}>
                                    <Option value="zScore">Z-Score</Option>
                                    <Option value="minMax">Min-Max</Option>
                                </Select>
                            </Form.Item>

                            <Form.Item
                                label="Regression Func"
                                name="regressionFunction"
                                rules={[{ required: false, message: 'Please input sample!' }]}
                            >
                                {/* this.state.regressionFuntion */}
                                <Text style={{ fontSize: 12, fontStyle: "italic" }}>{this.state.regressionFuntion ? <Button onClick={this.showModal} size="small" shape="round" danger>Show Function</Button> : "Not yet calculated"}</Text>
                                {/* <Input size="middle" style={{ width: 250 }} value = {this.state.regressionFuntion}/> */}
                            </Form.Item>


                            <Divider orientation="left">Range</Divider>

                            <Row style={{ display: "flex", flexDirection: "row", justifyContent: "space-evenly", alignContent: "center" }}>
                                <Col span={12}>
                                    <Form.Item wrapperCol={{
                                        xs: { span: 24 },
                                        sm: { span: 20 }
                                    }} name="x_from" rules={[{ validator: this.checkValueZero }]}>
                                        <Input value="3" addonBefore="X From" />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item wrapperCol={{
                                        xs: { span: 24 },
                                        sm: { span: 22 }
                                    }} name="x_to" rules={[{ validator: this.checkValueZero }]}>
                                        <Input defaultValue={0} addonBefore="X To" />
                                    </Form.Item>
                                </Col>
                            </Row>


                            <Row style={{ display: "flex", flexDirection: "row", justifyContent: "space-evenly", alignContent: "center", marginTop: 30 }}>
                                <Col span={12}>
                                    <Form.Item wrapperCol={{
                                        xs: { span: 24 },
                                        sm: { span: 20 }
                                    }} name="y_from" rules={[{ validator: this.checkValueZero }]}>
                                        <Input value={3} addonBefore="Y From" />
                                    </Form.Item>
                                </Col>
                                <Col span={12}>
                                    <Form.Item wrapperCol={{
                                        xs: { span: 24 },
                                        sm: { span: 22 }
                                    }} name="y_to" rules={[{ validator: this.checkValueZero }]}>
                                        <Input defaultValue={0} addonBefore="Y To" />
                                    </Form.Item>
                                </Col>
                            </Row>
                            {/* 
                        <Divider orientation="left">Outlier</Divider>
                        <Row style={{ display: "flex", flexDirection: "row", justifyContent: "space-evenly", alignContent: "center", marginTop: 30, marginLeft: 12 }}>
                            <Col span={10}>
                                <Form.Item name="x_outliner">
                                    <Input addonBefore="X outlier" defaultValue="0" />

                                </Form.Item>
                            </Col>
                            <Col span={10}>
                                <Form.Item name="y_outliner">
                                    <Input addonBefore="Y outlier" defaultValue="0" />
                                </Form.Item>
                            </Col>
                        </Row> */}

                            <Form.Item {...tailLayout} style={{ marginTop: 40 }}>
                                <Button type="primary" htmlType="submit">
                                    Generate
                        </Button>
                                {false ? <Progress percent={this.state.percent} /> : null}
                            </Form.Item>
                            {this.state.csvData.length > 0 ? <CSVLink data={this.state.csvData} headers={this.state.headers}
                                filename={"data_" + new Date().getTime() + ".csv"}
                                target="_blank" separator={";"}
                                ref={this.csvLink}
                                className="hidden"
                            >
                                Download CSV File
                        </CSVLink> : null}

                        </Form>
                    </Card>

                </Spin>
                <Modal
                    title="Regression Function Detail"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    {this.state.regressionFuntion}
                </Modal>
            </Container>
        }
    });

export default DataCreator;




//y = ß0 + ß1(X1) + ß2(X3) + ß3(X4)  