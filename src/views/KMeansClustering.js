import React from "react";
import { Form, Button, Card, InputNumber, Divider, Progress, notification, Spin, Switch } from 'antd';
import { Container } from 'reactstrap';
import { observer } from 'mobx-react';
import KMeansClusteringStore from "../stores/KMeansClusteringStore";


const openNotificationWithIcon = (type, message, duration = 5, title = 'Nice!') => {
    notification.config({
        placement: 'bottomRight',
        top: 20,
        duration: duration,
    })
    notification[type]({
        message: title,
        description:
            message
    });
};


const KMeansClustering = observer(
    class KMeansClustering extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                isSpinning: false,
                dataset: [],
                clusterJson: [
                    {
                        "cluster": 0,
                        "elements": []
                    }
                ],
                clusterCenters: [],
                threshold: 1,
                isUpdated: true,
                basedOnAverage: true
            };
        }

        csvLink = React.createRef()

        static defaultProps = {
            ...this.props
        }

        randomIntFromInterval = (min, max) => { // min and max included 
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min;
        }

        randomFloatFromInterval = (min, max) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            return (Math.random() * (max - min)) + min;
        }


        componentDidMount = () => {

        }

        checkValueZero = (rule, value) => {
            if (value < 0 || value > 0) {
                return Promise.resolve();
            }
            return Promise.reject(rule.field + ' must be greater or less than than zero!');
        };

        checkValueLessThanZero = (rule, value) => {
            if (value < 0) {
                return Promise.resolve();
            }
            return Promise.reject(rule.field + ' must be greater than zero!');
        };


        onFinish = values => {
            if (values) {
                //KMeansClusteringStore.setChartRendered(false);
                KMeansClusteringStore.setClusterJson([])
                KMeansClusteringStore.setKValue(Number(values.k_value))
                KMeansClusteringStore.setClusterCenters([], 0)
                console.log(this.state.basedOnAverage);
                if (Number(values.minimumRange) >= Number(values.maximumRange)) {
                    openNotificationWithIcon("error", "Max values must be greater than Min values.", "Hey!")
                }
                else {
                    try {
                        setTimeout(() => {
                            this.startToCluster();


                        }, 300);
                    } catch (error) {
                        openNotificationWithIcon("error", error)
                    }
                }
            }
        };

        onFinishFailed = errorInfo => {
            this.setState({ showProgress: false })
            console.log('Failed:', errorInfo);
        };

        startToCluster = () => {

            var dataset = []

            this.state.splittedData.map(dualData => {
                if (dualData.trim().length > 0) {
                    dualData = dualData.split(/(\s+)/).filter(e => e.trim().length > 0)
                    dataset.push(dualData);
                    return dualData;
                }
                else {
                    return dualData;
                }
            })

            this.setState({
                dataset
            })

            this.clustering();
        }

        showFile = async (e) => {
            e.preventDefault()
            this.setState({
                isSpinning: true,
            })
            const reader = new FileReader()
            reader.onloadend = async (e) => {
                const text = (e.target.result)
                console.log(text.split('\n'))

                try {
                    var splittedData = text.split('\n')
                    this.setState({
                        splittedData
                    })
                } catch (error) {
                    openNotificationWithIcon("warning", "This file could not opened or read", 5, "Oops")
                }

                this.setState({
                    isSpinning: false,
                })
            };
            console.log(e.target.files)
            if (e.target.files.length > 0) {
                if (!e.target.files[0].name.includes('.txt') || e.target.files[0].type !== 'text/plain') {
                    this.setState({
                        isSpinning: false
                    })
                    openNotificationWithIcon("warning", "Please select a .txt file !", 5, "Oops")                    
                }
                else {
                    reader.readAsText(e.target.files[0])
                }
            } else {
                this.setState({
                    isSpinning: false
                })
                openNotificationWithIcon("warning", "Please select a .txt file !",5, "Hey")
            }
        }

        setInitialClusterPoints = (k) => {
            let kClusterList = [];
            let selectedIndexes = [];

            for (let index = 0; index < k; index++) {
                var randIndex = this.randomIntFromInterval(0, this.state.dataset.length);

                if (!selectedIndexes.includes(randIndex)) {
                    kClusterList.push(this.state.dataset[randIndex])
                    selectedIndexes.push(randIndex);
                    console.log("random index: ", randIndex, "  C", (index + 1), " : ", this.state.dataset[randIndex])
                }
                else {
                    index--;
                }
            }

            console.log("Cluster Points:", kClusterList)
            this.setState({
                clusterCenters: kClusterList
            })
            return kClusterList;
        }

        findDistances = (clusterCenters) => {
            //(x1-x2)^2 + (y1-y2)^2 karekökü

            //her bir k değeri için ayrı bir liste olmalı

            let calculatedClustersWithDistances = [];
            let totalClusterDistanceList = 0;

            clusterCenters.forEach(cluster => {
                var clusterDistances = [];
                let totalDistance = 0;

                this.state.dataset.forEach(dualPoint => {
                    let innerResult = Math.pow((dualPoint[0] - cluster[0]), 2) + Math.pow((dualPoint[1] - cluster[1]), 2);

                    var distance = Math.sqrt(innerResult)//, 0.5);
                    totalDistance += distance;

                    //console.log("x1: ", dualPoint[0], " - x2:", cluster[0], "AND y1:", dualPoint[1], " - y2:", cluster[1], ".... Distance: ", distance)
                    clusterDistances.push(distance);
                });

                calculatedClustersWithDistances.push(clusterDistances);
                totalClusterDistanceList += totalDistance;
            });

            console.log("Total distance:", totalClusterDistanceList)

            var oldSSEValue = this.state.totalClusterDistanceList;

            this.setState({
                isUpdated: oldSSEValue !== totalClusterDistanceList,
                totalClusterDistanceList
            })

            console.log("Clusters With Distances:", calculatedClustersWithDistances);

            return calculatedClustersWithDistances;
        }

        clustering = () => {
            var k = KMeansClusteringStore._kValue
            let iteration = 1;
            let isUpdated = this.state.isUpdated

            while (isUpdated) {
                console.log("While iterasyon no:", iteration)

                if (iteration === 1) {
                    var clusterPoints = this.setInitialClusterPoints(k);
                    this.setState({
                        clusterCenters: clusterPoints
                    })
                }

                let distances = this.findDistances(this.state.clusterCenters);

                if (this.state.isUpdated) {
                    this.findMinimumDistanceAndAssignToCluster(distances)
                    this.refindNewCenterOfClusters();

                    iteration++;
                } else {

                    break;
                }
            }

            KMeansClusteringStore.setIterationCount(iteration);
            KMeansClusteringStore.setClusterCenters(this.state.clusterCenters, this.state.totalClusterDistanceList)
            KMeansClusteringStore.setClusterJson(this.state.clusterJson)

            this.setState({
                isSpinning: false,
                isUpdated: true
            })
        }

        refindNewCenterOfClusters = () => {

            if (this.state.basedOnAverage) {

                var tempClusterCenters = [];

                let clusterDistanceList = [];

                this.state.clusterJson.forEach((clusterInfo, index) => {
                    var xTotal = 0, yTotal = 0;
                    //let clusterDistance = 0;

                    clusterInfo.elements.forEach(element => {
                        xTotal += Number(element.points[0]);
                        yTotal += Number(element.points[1]);
                    })

                    var element = [xTotal / clusterInfo.elements.length, yTotal / clusterInfo.elements.length];
                    tempClusterCenters.push(element);
                    //clusterDistanceList.push(clusterDistance);
                })
                console.log("Ortalamaya Göre new centers:", tempClusterCenters, "clusterDistnce List :", clusterDistanceList)

                this.setState({
                    clusterCenters: tempClusterCenters
                })
            } else {

                let clusterCenterList = [];

                for (let i = 0; i < this.state.clusterJson.length; i++) {

                    var initialCenter = [Number(this.state.clusterJson[i].elements[0].points[0]), Number(this.state.clusterJson[i].elements[0].points[1])];
                    var bestMinDistance = 0;
                    let centerPoint = [];
                    for (let j = 0; j < this.state.clusterJson[i].elements.length; j++) {
                        let innerResult = Math.pow((Number(this.state.clusterJson[i].elements[j].points[0]) - initialCenter[0]), 2) + Math.pow((Number(this.state.clusterJson[i].elements[j].points[1]) - initialCenter[1]), 2);

                        var distance = Math.sqrt(innerResult);

                        if (distance < bestMinDistance || j === 0) {
                            bestMinDistance = distance
                            centerPoint = [Number(this.state.clusterJson[i].elements[j].points[0]), Number(this.state.clusterJson[i].elements[j].points[1])]
                        }
                    }
                    clusterCenterList.push(centerPoint)
                }


                console.log("Orta Nokta new centers:", clusterCenterList)

                this.setState({
                    clusterCenters: clusterCenterList
                })
            }
        }

        findMinValueOfAList = (list = []) => {
            let min = list[0], indexOfMinVal = 1

            for (let i = 0; i < list.length; i++) {
                let value = list[i]

                //min = (value < min) ? value : min

                if (value < min) {
                    min = value
                    indexOfMinVal = i + 1;
                }
            }

            return { clusterIndex: indexOfMinVal, minValue: min };
        }

        prepareClusterJSON = (clusterCount) => {
            var clusterJson = [];
            for (let index = 1; index <= clusterCount; index++) {
                let innerObject = {};
                innerObject.cluster = index;
                innerObject.elements = [];

                clusterJson.push(innerObject);
            }

            this.setState({
                clusterJson
            })
        }

        findMinimumDistanceAndAssignToCluster = (distances = []) => {
            this.prepareClusterJSON(distances.length);

            let oldClusterJson = this.state.clusterJson;

            for (let index = 0; index < this.state.dataset.length; index++) {
                var temporaryCompareList = [];

                for (let j = 0; j < distances.length; j++) {
                    temporaryCompareList.push(distances[j][index])
                }

                let result = this.findMinValueOfAList(temporaryCompareList);

                let temporaryClusterJson = this.state.clusterJson;
                let clusterInfo = temporaryClusterJson.find(c => c.cluster === result.clusterIndex);

                if (clusterInfo) {

                    var innerElementPoints = clusterInfo.elements;

                    var elementsObject = {
                        "index": index,
                        "points": this.state.dataset[index]
                    }

                    innerElementPoints.push(elementsObject);

                    this.setState({
                        clusterJson: temporaryClusterJson
                    })

                }
            }
            console.log("isUpdated:", JSON.stringify(this.state.clusterJson) !== JSON.stringify(oldClusterJson))
            console.log("RESULT :", this.state.clusterJson)
        }

        render() {
            const layout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 16 }
                },
            };

            const tailLayout = {
                wrapperCol: {
                    xs: {
                        span: 16,
                        offset: 0,
                    },
                    sm: {
                        span: 16,
                        offset: 4,
                    },
                }
            };

            return <Container style={{ ...this.props.style }}>
                <Spin spinning={this.state.isSpinning} tip="Calculating... Please Wait">
                    <Card style={{ width: "100%" }}>
                        <Form hideRequiredMark="true" {...layout} name="basic" initialValues={{ remember: true, x_from: 0, x_to: 0, y_from: 0, y_to: 0 }} onFinishFailed={this.onFinishFailed} onFinish={(values) => {
                            this.setState({
                                isSpinning: true,
                            })
                            this.onFinish(values)
                        }} >
                            <Divider orientation="left">k Means Clustering</Divider>
                            <div style={{ margin: "24px 0px 24px 24px" }}>
                                <input type="file" onChange={(e) => this.showFile(e)} />
                            </div>
                            <Form.Item
                                label="Cluster Count"
                                name="k_value"
                                required
                                tooltip="This is a required field"
                                rules={[{ required: true, message: 'Please input Cluster Count!' }]} >
                                <InputNumber size="middle" style={{ width: 250 }} min={2} max={50} defaultValue={5} />
                            </Form.Item>

                            <Form.Item
                                label="Center Selection" >

                                <Switch
                                    checkedChildren={"Based on Average"}
                                    unCheckedChildren={"Center Point"}
                                    defaultChecked
                                    onChange={(val) => {
                                        this.setState({
                                            basedOnAverage: val
                                        })
                                        // console.log(this.state.basedOnAverage)
                                    }}
                                />
                            </Form.Item>


                            <Form.Item {...tailLayout} style={{ marginTop: 40 }}>
                                <Button type="primary" htmlType="submit">
                                    Classify me!
                                </Button>
                                {false ? <Progress percent={this.state.percent} /> : null}
                            </Form.Item>

                        </Form>
                    </Card>

                </Spin>
            </Container>
        }
    });

export default KMeansClustering;