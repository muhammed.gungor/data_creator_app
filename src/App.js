import React from 'react';
import './App.css';
import ModulesLayout from './views/ModulesLayout';
import 'antd/dist/antd.css';

function App() {
  return (
    <div className="App">
      <ModulesLayout></ModulesLayout>      
    </div>
  );
}

export default App;
