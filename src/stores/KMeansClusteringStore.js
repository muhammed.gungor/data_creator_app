import { observable, decorate } from "mobx"

class KMeansClusteringStore {
    _iterationCount = 0;
    _clusterJson = [];
    _kValue = 0;
    _chartRendered = false;
    _clusterCenters = []
    _sseValue = 0;

    async setIterationCount(iterationCount) {
        this._iterationCount = iterationCount;
    }
    setChartRendered(chartRendered) {
        this._chartRendered = chartRendered
    }

    async setClusterCenters(centers,sse) {
        this._clusterCenters = centers
        this._sseValue = sse
    }

    async setClusterJson(clusterJson) {

        let chartDataSource = [];
        clusterJson.forEach(clusterInfo => {
            clusterInfo.elements.forEach(element => {
                var data = {
                    cluster: ("Cluster" + clusterInfo.cluster), x: Number(element.points[0]), y: Number(element.points[1]), isCenter: false
                }

                chartDataSource.push(data);
            })
        });

        this._clusterJson = chartDataSource
    }
    setKValue(kValue) {
        this._kValue = kValue
    }
}

decorate(KMeansClusteringStore, {
    _iterationCount: observable,
    _clusterJson: observable,
    _kValue: observable,
    _chartRendered: observable,
    _sseValue: observable
});

export default new KMeansClusteringStore()
