import { observable, decorate } from "mobx"

class SimulatedAnnealingStore {
    
    minValues = null;

    setMinValues(minValues) {
        this.minValues = minValues;
    }
}

decorate(SimulatedAnnealingStore, {
    minValues: observable
});

export default new SimulatedAnnealingStore()
